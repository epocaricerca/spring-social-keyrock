/*
 * Copyright 2015 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.springframework.social.keyrock.connect;

import static org.junit.Assert.*;

import java.util.Date;

import org.junit.Ignore;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.social.connect.UserProfile;
import org.springframework.social.keyrock.api.Keyrock;
import org.springframework.social.keyrock.api.KeyrockProfile;
import org.springframework.social.keyrock.api.UserOperations;

public class KeyrockAdapterTest {

	private KeyrockAdapter apiAdapter = new KeyrockAdapter();
	
	private Keyrock keyrock = Mockito.mock(Keyrock.class);
	
	@Test
	public void fetchProfile() {
		UserOperations userOperations = Mockito.mock(UserOperations.class);
		Mockito.when(keyrock.userOperations()).thenReturn(userOperations);
		Mockito.when(userOperations.getUserProfile()).thenReturn(new KeyrockProfile("damiano-fontana", "damiano_fontana", "damiano_fontana@epocaricerca.it"));
		UserProfile profile = apiAdapter.fetchUserProfile(keyrock);
		assertEquals("damiano_fontana@epocaricerca.it", profile.getName());
		assertEquals("damiano_fontana@epocaricerca.it", profile.getEmail());
		assertEquals("damiano_fontana", profile.getUsername());
	}

	
}
