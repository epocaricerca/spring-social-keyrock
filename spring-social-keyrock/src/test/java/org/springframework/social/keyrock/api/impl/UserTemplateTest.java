/*
 * Copyright 2015 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.springframework.social.keyrock.api.impl;

import static org.junit.Assert.*;
import static org.springframework.http.HttpMethod.*;
import static org.springframework.http.MediaType.*;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.*;
import static org.springframework.test.web.client.response.MockRestResponseCreators.*;

import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.springframework.social.keyrock.api.KeyrockProfile;


/**
 * @author Damiano Fontana
 */
public class UserTemplateTest extends AbstractKeyrockApiTest {

	
	@Test
	public void getUserProfile() throws Exception {
		mockServer.expect(requestTo("https://account.lab.fiware.org/user?access_token=KAzTMei7WwTo6ilKaOK3hhFe8hMv6Y"))
				.andExpect(method(GET))
				.andRespond(withSuccess(jsonResource("keyrock-profile"), APPLICATION_JSON));

		KeyrockProfile profile = keyrock.userOperations().getUserProfile();
		assertEquals("damiano-fontana", profile.getId());
	
	}
	
	

}
