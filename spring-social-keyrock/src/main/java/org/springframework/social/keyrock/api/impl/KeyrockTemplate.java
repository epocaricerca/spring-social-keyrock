package org.springframework.social.keyrock.api.impl;


/*
 * Copyright 2015 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


import java.io.IOException;
import java.util.List;

import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.social.NotAuthorizedException;
import org.springframework.social.UncategorizedApiException;
import org.springframework.social.keyrock.api.Keyrock;
import org.springframework.social.keyrock.api.UserOperations;
import org.springframework.social.oauth2.AbstractOAuth2ApiBinding;
import org.springframework.social.oauth2.OAuth2Version;
import org.springframework.social.oauth2.TokenStrategy;
import org.springframework.social.support.ClientHttpRequestFactorySelector;
import org.springframework.web.client.RestOperations;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;


/**
 * <p>This is the central class for interacting with Keyrock.</p>
 * <p>
 * There are some operations, such as searching, that do not require OAuth
 * authentication. In those cases, you may use a {@link KeyrockTemplate} that is
 * created through the default constructor and without any OAuth details.
 * Attempts to perform secured operations through such an instance, however,
 * will result in {@link NotAuthorizedException} being thrown.
 * </p>
 * @author Damiano Fontana
 */
public class KeyrockTemplate extends AbstractOAuth2ApiBinding implements Keyrock {

	private String appId;
	
	
	private ObjectMapper objectMapper;

	private String applicationNamespace;
	
	private UserOperations userOperations;

	/**
	 * Create a new instance of KeyrockTemplate.
	 * This constructor creates the FacebookTemplate using a given access token.
	 * @param accessToken An access token given by Keyrock after a successful OAuth 2 authentication.
	 */
	public KeyrockTemplate(String accessToken) {
		this(accessToken, null);
	}

	public KeyrockTemplate(String accessToken, String applicationNamespace) {
		this(accessToken, applicationNamespace, null);
	}
	
	public KeyrockTemplate(String accessToken, String applicationNamespace, String appId) {
		super(accessToken,TokenStrategy.ACCESS_TOKEN_PARAMETER);
		this.applicationNamespace = applicationNamespace;
		this.appId = appId;
		initialize();
	}
	
	@Override
	public void setRequestFactory(ClientHttpRequestFactory requestFactory) {
		// Wrap the request factory with a BufferingClientHttpRequestFactory so that the error handler can do repeat reads on the response.getBody()
		//super.setRequestFactory(ClientHttpRequestFactorySelector.bufferRequests(requestFactory));
	}

	
	// AbstractOAuth2ApiBinding hooks
	@Override
	protected OAuth2Version getOAuth2Version() {
		return OAuth2Version.DRAFT_10;
	}

	@Override
	protected void configureRestTemplate(RestTemplate restTemplate) {
	}

	@Override
	protected MappingJackson2HttpMessageConverter getJsonMessageConverter() {
		MappingJackson2HttpMessageConverter converter = super.getJsonMessageConverter();
		objectMapper = new ObjectMapper();				
		converter.setObjectMapper(objectMapper);		
		return converter;
	}
	
	// private helpers
	private void initialize() {
		//super.setRequestFactory(ClientHttpRequestFactorySelector.bufferRequests(getRestTemplate().getRequestFactory()));
		initSubApis();
	}
		
	private void initSubApis() {
		this.userOperations = new UserTemplate(getRestTemplate());
	}
	

	@Override
	public UserOperations userOperations() {

		return this.userOperations;
	}

	@Override
	public RestOperations restOperations() {
		return getRestTemplate();
	}
	
}