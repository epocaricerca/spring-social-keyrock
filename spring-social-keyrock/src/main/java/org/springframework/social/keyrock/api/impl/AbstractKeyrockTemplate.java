package org.springframework.social.keyrock.api.impl;

import org.springframework.web.client.RestTemplate;

public class AbstractKeyrockTemplate {

	protected final RestTemplate restTemplate;
		
	public AbstractKeyrockTemplate(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
		
	}

	protected String getApiBaseUrl()
	{
		return "https://account.lab.fiware.org/";
	}
	
}
