/*
 * Copyright 2015 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.springframework.social.keyrock.connect;

import org.springframework.social.connect.support.OAuth2ConnectionFactory;
import org.springframework.social.keyrock.api.Keyrock;


/**
 * Keyrock ConnectionFactory implementation.
 * @author Damiano Fontana
 */
public class KeyrockConnectionFactory extends OAuth2ConnectionFactory<Keyrock> {

	/**
	 * Creates a KeyrockConnectionFactory for the given application ID and secret.
	 * Using this constructor, no application namespace is set .
	 * @param appId The application's App ID as assigned by Keyrock 
	 * @param appSecret The application's App Secret as assigned by Keyrock
	 */
	public KeyrockConnectionFactory(String appId, String appSecret) {
		this(appId, appSecret, null);
	}

	/**
	 * Creates a KeyrockkConnectionFactory for the given application ID, secret, and namespace.
	 * @param appId The application's App ID as assigned by Keyrock 
	 * @param appSecret The application's App Secret as assigned by Keyrock
	 */
	public KeyrockConnectionFactory(String appId, String appSecret, String appNamespace) {
		super("keyrock", new KeyrockServiceProvider(appId, appSecret, appNamespace), new KeyrockAdapter());
	}
	
}
