/*
 * Copyright 2015 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.springframework.social.keyrock.connect;

import org.springframework.social.ApiException;
import org.springframework.social.connect.ApiAdapter;
import org.springframework.social.connect.ConnectionValues;
import org.springframework.social.connect.UserProfile;
import org.springframework.social.connect.UserProfileBuilder;
import org.springframework.social.keyrock.api.Keyrock;
import org.springframework.social.keyrock.api.KeyrockProfile;


/**
 * Keyrock ApiAdapter implementation.
 * @author Damiano Fontana
 */
public class KeyrockAdapter implements ApiAdapter<Keyrock> {

	public boolean test(Keyrock keyrock) {
		try {
			keyrock.userOperations().getUserProfile();
			return true;
		} catch (ApiException e) {
			return false;
		}
	}

	public void setConnectionValues(Keyrock keyrock, ConnectionValues values) {
		KeyrockProfile profile = keyrock.userOperations().getUserProfile();
		values.setProviderUserId(profile.getId());
		values.setDisplayName( profile.getDisplayName());
		
	}

	public UserProfile fetchUserProfile(Keyrock keyrock) {
		KeyrockProfile profile = keyrock.userOperations().getUserProfile();
		return new UserProfileBuilder().setName(profile.getEmail()).setUsername(profile.getDisplayName()).setEmail(profile.getEmail()).build();
	}
	
	public void updateStatus(Keyrock keyrock, String message) {
		
	}
	
}
