/**
 * Spring Social Keyrock's XML configuration namespace.
 */
package org.springframework.social.keyrock.config.xml;
